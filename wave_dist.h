#ifndef WAVE_DIST_H
#define WAVE_DIST_H
typedef double real;


//Initialise the results array to 0.0
void waveResultsinit(const real results[10][5]);

//Returns the parameter value Lambda for a root of f(x)=h(x)-mx-b relative to the points a and b,
inline real waveLambdaj(const real xr, const real bx, const real ax);

//computes upper bound for number of roots
void  waveRootsn(const real points[], const real params[],real roots[10][5]);

//returns the coefficient lambda(x) for a specific point
inline real waveLambda(const real x, const real beta, const real params[], const real points[]);

//returns the coefficient beta(x) for a specific point
inline real waveBeta(const real x, const real m, const real arams[], const real points[]);

///returns the slope of the segment
inline real waveSlope(const real points[]);

///returns the slope of the segment
inline real waveIntercept(const real xa, const real ya,const real m);

///calculates the value x for which -beta(x) is a minimun therefore beta(x) is maxima using the Newton method
static real waveMin(const real interval[],const real params[],const real m, const real xo);


//Test for beta, beta' and beta''
void waveNRtest(const real interval[],const real params[],const real m);

/// calculates the factor lx on the segment after finding the maxima bx of the distance from the segment defined
/// by (p1x,p1y) and (p2x,p2y) and the inward normals of the curve Acos(kx+phi)
void  waveDist(const real interval[], const real params[], real coeffs[],const real m);



#endif
