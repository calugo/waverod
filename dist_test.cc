#include <iostream>
#include <cstdio>
#include <cmath>
#include "wave_dist.h"
typedef double real;

int main(int argc, char** argv)
{
  if (argc < 3){
	printf("You have entered %d arguments\n", argc);
  return 1;
  }
  //////////////////
  else{
  real pts[4],params[3],results[10][5];
  real *coeffs;
  if (atof(argv[4])<atof(argv[6])){
    pts[0]=atof(argv[4]); pts[1]=atof(argv[5]); pts[2]=atof(argv[6]); pts[3]=atof(argv[7]);
  }
  else{
    pts[0]=atof(argv[4]); pts[1]=atof(argv[5]); pts[2]=atof(argv[6]); pts[3]=atof(argv[7]);
  }
  params[0]=atof(argv[1]);
  params[1]=2*M_PI/atof(argv[2]);
  params[2]=atof(argv[3]);

  waveRootsn(pts,params,results);

  printf("INTERVALS:\n");
  printf("[R1,R2]\n");
  FILE *fp;
  fp = fopen("results.txt", "w");//opening file

  for(int i=0;i<10;i++){
    if (results[i][0]==0.0 && results[i][1]==0.0)
    {
      break;
    }
    else {
      printf("%lf %lf %lf %lf %lf\n",results[i][0],results[i][1],results[i][2],results[i][3],results[i][4]);
      fprintf(fp,"%lf %lf %lf %lf %lf\n",results[i][0],results[i][1],results[i][2],results[i][3],results[i][4]);
    }
  }
  fclose(fp);
  printf("DONE\n");
  return 0;
}
}
