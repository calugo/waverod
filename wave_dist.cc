// Cytosim was created by Francois Nedelec. Copyright 2022 Cambridge University.
#include <cmath>
#include <cfloat>
#include <cstdio>
typedef double real;



/**
Initialises the results array to zeroes
*/
void waveResultsinit(real results[10][5]){
  for (int i=0;i<10;i++){
    for(int j=0;j<4;j++)
    results[i][j]=0.0;
  }
}

/**
Calculates the factor lj for an intesection point p=(p1,p2) of the segment
spanned by the points a and b with the surface h=Acos(kx+phi).
The factor is defined by the parametrisation p=a+lj(b-a) and 0<=lj<=1.
a=(ax,ay), b=(bx,by).
This leads to the equality ax+lj(bx-ax)=px, px is a root of h-mx+b and it
is calculated elsewhere. Therefore:
lj=(px-ax)/(bx-ax)
The arguments are the root and the x components bx and ax of the segment.
*/

inline real waveLambaj(const real xr, const real bx, const real ax){
  return (xr-ax)/(bx-ax);
}

/**
Calculates the slope of the segment defined by points={xa,ya,xb,yb}
*/

inline real waveSlope(const real points[4]){
  return (points[3]-points[1])/(points[2]-points[0]);
}

/**
Calculates de y-intercept b of mx+b as b=ya-m*xa taking xa,ya and m as arguments
*/
inline real waveIntercept(const real xa, const real ya,const real m){
  return (ya-m*xa);
}

/**
Computes the normal coeffiecient beta=(1/(1+mh'))*(tau(h(x)-ay+m(ax-x)))
Where tau=sqrt(1+[h']^2)
m=(by-ay)/(bx-ax)
h=Acos(kx+phi)
h'=-Aksin(kx+phi)
It takes x, m, params={A,k,phi} and points={ax,ay,bx,by} as arguments
*/
inline real waveBeta(const real x, const real m, const real params[3], const real points[4]){
  const real h=params[0]*cos(params[1]*x+params[2]);
  const real dh=-params[0]*params[1]*sin(params[1]*x+params[2]);
  const real tau=sqrt(1+dh*dh);
  const real A=tau*(h-points[1]+m*(points[0]-x));
  const real B=1+m*dh;
  return A/B;
}


/**
Computes the factor coeffiecient lg=(1/(by-ay))*(h-ay-beta/tau) over the segment given x and beta
It take x, beta, params={A,k,phi} and points={ax,ay,bx,by} as arguments
the quantity tau=sqrt(1+[h']^2) is calculated. Where h'=-Aksin(kx+phi)
*/
inline real waveLambda(const real x, const real beta, const real params[3], const real points[4]){
  const real h=params[0]*cos(params[1]*x+params[2]);
  const real dh=-params[0]*params[1]*sin(params[1]*x+params[2]);
  const real tau=sqrt(1+dh*dh);
  const real A=h-points[1]-(beta/tau);
  const real B=points[3]-points[1];
  return A/B;
}

////////////////
/**
Test for discrepancies with other code, this is not called anywhere and can be ignored or erased once it
is implemented.
*/
 void waveNRtest(const real interval[],const real params[],const real m){
    real xn, xmin, H;
    int itnum;

    real angle,h,dh,ddh,dddh;
    real tau,dtau,ddtau;
    real num, den, Z,dZ,ddZ;
    real u,du,ddu,dx;
    bool conv=true;
    H=1;
    itnum=0;
    xn=interval[0];
    dx=(interval[2]-interval[0])/1000;
    FILE *fp;
    fp = fopen("ududdu.txt", "w");//opening file


    while (xn <= interval[2]){

      angle=params[1]*xn+params[2];
      h=params[0]*cos(angle);
      dh=-params[0]*params[1]*sin(angle);
      ddh=-params[1]*params[1]*h;
      dddh=-params[1]*params[1]*dh;

      tau=sqrt(1+dh*dh);
      dtau=(dh*ddh)/tau;
      ddtau=(ddh*ddh+dh*dddh)/(tau*tau);

      //printf("%lf %lf %lf\n",angle,h,tau);

      num=(1+m*dh);
      den=1/(1+m*dh);

      Z=den*tau;
      dZ=(den*dtau)-tau*m*ddh*den*den;
      ddZ=ddtau*den - den*den*m*(2*dtau*ddh - tau*dddh) + 2*den*den*den*tau*m*m*ddh*ddh;

      u=Z*(interval[1]-h+m*(xn-interval[0]));
      du = dZ*( m*xn -h + interval[1] - m*interval[0]) + Z*(m-dh);
      ddu = ddZ*( m*xn - h + interval[1] - m*interval[0]) + 2*dZ*(m-dh) - ddh*Z;

      H=du/ddu;


      fprintf(fp,"%lf %lf %lf %lf %lf\n",xn,u,du,ddu,fabs(H));

      //printf("du:%lf ddu:%lf du/ddu:%lf H:%lf\n",du,ddu,du/ddu,H);
      xn=xn+dx;
    }
    fclose(fp);
}
////////////////

/**
Newton method to calculate the minima of -beta(x).
Which is the is maxima of beta(x) in the segment below the wave, it takes the roots and the slope alongside
the wave parameters
*/
static real waveMin(const real interval[],const real params[],const real m, const real xo){
    real xn, xmin, H;
    int itnum;
    xmin=0.0;
    real angle,h,dh,ddh,dddh;
    real tau,dtau,ddtau;
    real num, den, Z,dZ,ddZ;
    real du,ddu;
    bool conv=true;
    H=1;
    itnum=0;
    xn=xo;

    while (fabs(H) > FLT_EPSILON){

      angle=params[1]*xn+params[2];
      h=params[0]*cos(angle);
      dh=-params[0]*params[1]*sin(angle);
      ddh=-params[1]*params[1]*h;
      dddh=-params[1]*params[1]*dh;

      tau=sqrt(1+dh*dh);
      dtau=(dh*ddh)/tau;
      ddtau=(ddh*ddh+dh*dddh)/(tau*tau);


      num=(1+m*dh);
      den=1/(1+m*dh);

      Z=den*tau;
      dZ=(den*dtau)-tau*m*ddh*den*den;
      ddZ=ddtau*den - den*den*m*(2*dtau*ddh - tau*dddh) + 2*den*den*den*tau*m*m*ddh*ddh;

      du = dZ*( m*xn -h + interval[1] - m*interval[0]) + Z*(m-dh);
      ddu = ddZ*( m*xn - h + interval[1] - m*interval[0]) + 2*dZ*(m-dh) - ddh*Z;

      H=du/ddu;
      xn=xn-H;
      xmin=xn;
      itnum+=1;


      if ((itnum>100) || (xn<interval[0]) || (xn>interval[2])){
          //if (xn<interval[0]){ xmin=interval[0];}
          //if (xn>interval[2]){ xmin=interval[2];}
          if (itnum>100){
             printf("Did not converge after 100 iterations\n");
             xmin=0.0;
             conv = false;
             break;
            }
          //break;
        }
    }

    if (conv){
        printf("Converged after %d iterations\n",itnum);
        printf("NRXMIN: %lf\n",xmin);
    }

    return xmin;
}

/**
The function receives two points which are a pair of consecutive roots xj={x1,x2}
(xj,yj=mxj+b)=(xj,Acos(kxj+phi)) and the parameters (A,k,phi). The points
constitute the first argument pts={x1,y1,x2,y2} and the parameters are in the
second argument params={A,k,phi}.

Then it minimise the distance 'beta' from the surface to the line in the direction
of the normal N=(1/sqrt(1+ [dh/dx]^2))*(dh/dx,-1). The minima is found
using the the newton function.
*/

void waveDist(const real interval[], const real params[], real coeffs[], const real m){

  int  cnt=0;
  real xmin,xo;
  real bmax,bj,b1,b2,xm;


  bmax=0;
  b1=waveBeta(interval[0],m,params, interval);
  b2=waveBeta(interval[2],m,params, interval);



  if (b1>b2){
      bmax=b1;
      xm=interval[0];
    }
  else{
      bmax=b2;
      xm=interval[2];
    }

  for(int j=0;j<=5;j++){
      xo=interval[0]+(j/5.0)*(interval[2]-interval[0]);
      xmin=waveMin(interval,params,m,xo);
      waveNRtest(interval,params, m);
      bj=waveBeta(xmin,m,params, interval);
      if (bj>bmax){
        bmax=bj;
        xm=xmin;
      }
  }

  coeffs[0]=xm;
  coeffs[1]=bmax;
}


/**
The projection of two points a=(ax,ay) and b=(bx,by) in points={ax,ay,bx,by}  on
the x axis has a length l=|bx-ax|.

The function h=Acos(kx+phi) defined by the parameters param=(A,k,phi)
has a wavelength lambda=2pi/k.

The intersections points are estimated  in this function by walking along the segment and comparing
y1=mx+b-h(x) and y2=m(x+dx)-h(x+dx).

If y1>0 and y2<0 or y1<0 and y2>0 we keep the value
x or x+dx respectively.

The values at the extremes are kept if they are below the curve.
*/

void waveRootsn(const real points[4], const real params[3], real results[10][5]){

  const real gamma=(params[1]*(points[2]-points[0]))/2*M_PI;

  const real dx=0.001*(2*M_PI/params[1]);
  real x=points[0];
  int cnt=0,nrp;
  real y1,y2,r1,r2,l1,l2,l;
  real coeffs[3],interval[4];

  waveResultsinit(results);

   if ((points[1]<params[0])||(points[3]<params[0])){
     const real m=waveSlope(points);
     const real b=waveIntercept(points[0],points[1],m);
     printf("m:%lf b:%lf\n",m,b);

     if( (m*points[0]+b)< (params[0]*cos(params[1]*points[0]+params[2]))){
       r1=points[0];
       cnt+=1;
       }

    x=points[0];

    while (x<=points[2]){
       nrp=0;
       y1=m*x+b-params[0]*cos(params[1]*x+params[2]);
       y2=m*(x+dx)+b-params[0]*cos(params[1]*(x+dx)+params[2]);
       if (y1<0 && y2>0){
         cnt+=1;
         if (cnt%2==0){r2=x;nrp=1;}
         if (cnt%2==1){r1=x;}
       }

       if (y1>0 && y2<0){
         cnt+=1;
         if (cnt%2==0){r2=x+dx;nrp=1;}
         if (cnt%2==1){r1=x+dx;}
       }

       if ((x+dx>=points[2]) && y2<0 ){r2=points[2];cnt+=1;nrp=1;}

       x+=dx;
       if ( cnt%2==0 && nrp==1 ){
         results[int(0.5*(cnt-1))][0]=r1;
         results[int(0.5*(cnt-1))][1]=r2;
         interval[0]=r1;
         interval[1]=m*r1+b;
         interval[2]=r2;
         interval[3]=m*r2+b;
         //I used this to make some tests and plots, Im keeping it here in case more tests are needed.
         //it has to go in the final version
         //FILE *fp;
         //fp = fopen("pointsint.txt", "w");//opening file
         //fprintf(fp,"%lf %lf %lf %lf\n",interval[0],interval[1],interval[2],interval[3]);
         //fclose(fp);

         //printf("POINTS C:\n");
         //printf("%lf %lf %lf %lf\n",interval[0],interval[1],interval[2],interval[3]);
         waveDist(interval,params, coeffs, m);
         results[int(0.5*(cnt-1))][2]=coeffs[0];
         results[int(0.5*(cnt-1))][3]=coeffs[1];
         l1=waveLambaj(r1, points[2], points[0]);
         l2=waveLambaj(r2, points[2], points[0]);
         l= waveLambda(coeffs[0], coeffs[1], params, interval);
         results[int(0.5*(cnt-1))][4]=l1+l*(l2-l1);
       }
    }
 }

}
