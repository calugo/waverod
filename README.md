# Distance to a rod under a periodic curve

![alt text](GPIC.png "Title")

The problem is to calculate the scale factors $`\lambda`$ and $`\beta`$ for the maximum distance between the segment defined by the points $`\bf{a}`$ and $`\bf{b}`$ and which is below the curve $`{\bf{h}}=(x,h)`$. In this case $`h(x)=A\cos(kx+\phi)`$.

To do so the first thing to do the steps to follow are:

1. Determine if the there are segments below the curve which means to obtain the roots of $`g(x)=l-h`$ for $`l=m a_x+ b`$ with $`m=\frac{b_y-a_y}{b_x-a_x}`$ and $`b=a_y-ma_x`$.

2. If there are roots $`\{x_k\}_{k=1} ^M`$ for each pair $`(x_k, x_{k+1})`$ the points $`\bf{p_1}`$ and $`\bf{p_2}`$ are computed as $`{\bf{p}_j}=(x_j,mx_j+b)`$.

3. For each par of points $`{\bf{p_1}},{\bf{p_2}}`$, the relationships for $`\lambda`$ and $`\beta`$ to minimise are obtained from $`{\bf{r}}={\bf{p_1}+\lambda({\bf{p_2-p_1}}})`$ and $`{\bf{r}}={\bf{h}}+\beta{\bf{\hat{N}}}`$ with $`{\bf{\hat{N}}}=\frac{1}{\sqrt{1+h'^2}}(h',-1)`$ (In-ward unit normal). Which lead to:

```math
\beta=\frac{\tau \left(h-p_{1y}+m(p_{1x}-x)\right)}{1+mh'}
```
and
```math
\lambda=\frac{1}{p_{2y}-p_{1y}}\left(h-p_{1y}-\frac{\beta}{\tau}\right)
```

Where $`\tau=\sqrt{1+h'^2}`$.

4. Using Newton's method for finding minima we compute the extreme values of $`\beta`$. Then evaluate $`\lambda`$.

The solution of the problem is obtained as follows:

* The programs verify if any of the two extremes is less than the amplitude of the surface.
* If one of the extremes is effectively below the Amplitude, then it checks if there is a set of intersections.
* If the number of pairs of roots is larger than zero then the Newton method is called to obtain the values for which $`- \beta`$ is a local minima.
* Then the minima of minimas is kept.
* Computes the values `\lambda` over the segment corresponding to the minima and with respect to the point a.
* It computes up to 10 pairs of roots. 



# Compilation:

`> g++ -o wdtest dist_test.cc wave_dist.cc`

# Execution

The executable takes the arguments Amplitude, wavlength, phase, $`a_x`$, $`a_y`$, $`b_x`$, $`b_y`$, for instance:

`./wdtest 0.5 3.8 0.7853981633974483 -1.5 0.166 1.5 0.55`
